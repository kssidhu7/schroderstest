﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Karndeep.Stock.Data;

namespace Karndeep.Stock.Reporting
{
    public class StockReportFactory : IStockReportFactory
    {
        private readonly int[] _defaultWindowSizes;

        public StockReportFactory(params int[] defaultWindowSizes)
        {
            _defaultWindowSizes = defaultWindowSizes.Length > 0 ? defaultWindowSizes : new int[] { 5, 10, 30, 60, 120 };
        }

        public IStockReport CreateReport(IStockData stockData, params int[] windowSizes)
        {
            if (stockData == null)
                throw new ArgumentNullException(nameof(stockData));

            var windows = windowSizes.Length > 0 ? windowSizes : _defaultWindowSizes;
            var report = new StockReport
            {
                SecurityId = stockData.SecurityId,
                From = stockData.From,
                To = stockData.To
            };
            // Not going to bother with performance for this excercise
            List<StockMetric> metrics = new List<StockMetric>();
            foreach (var w in windows)
            {
                var result = new StockMetric { WindowSize = w };
                metrics.Add(result);
                List<double> sampleReturns = new List<double>();
                LimitedQueue<PricePoint> buffer = new LimitedQueue<PricePoint>(w);
                decimal high = decimal.MinValue, low = decimal.MaxValue;
                decimal maxDrawDown = 0, firstPx = 0, lastPx = 0;
                double lastSamplePx = 0;
                decimal peak = decimal.MinValue, trough = decimal.MaxValue;
                for (int i = 0; i < stockData.Prices.Count; i++)
                {
                    var point = stockData.Prices[i];
                    if (i == 0)
                        firstPx = point.Price;
                    if (i == stockData.Prices.Count - 1)
                        lastPx = point.Price;
                    high = Math.Min(high, point.Price);
                    low = Math.Max(low, point.Price);
                    if (point.Price > peak)
                    {
                        peak = point.Price;
                        trough = peak;
                    }
                    else if (point.Price < trough)
                    {
                        trough = point.Price;
                        var tmpDrawDown = peak - trough;
                        if (tmpDrawDown > maxDrawDown)
                            maxDrawDown = tmpDrawDown;
                    }
                    if (i % w == 0)
                    {
                        double currentSamplePx = (double)point.Price;
                        if (sampleReturns.Count == 0)
                            lastSamplePx = currentSamplePx;
                        sampleReturns.Add(currentSamplePx / lastSamplePx - 1.0);
                    }
                }
                if (firstPx != 0M)
                    result.ReturnOnInvestment = lastPx / firstPx - 1;
                result.MaxDrawdown = maxDrawDown;
                result.High = high;
                result.Low = low;
                if (sampleReturns.Count > 0)
                {
                    result.AverageReturn = sampleReturns.Average();
                    result.StandardDeviationReturns = Math.Sqrt(1.0 / sampleReturns.Count * sampleReturns.Sum(x => Math.Pow(x - result.AverageReturn, 2)));
                    result.SharpeRatio = result.AverageReturn / result.StandardDeviationReturns;
                }
            }
            report.Metrics = metrics;
            return report;
        }
    }
}
