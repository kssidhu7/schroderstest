﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Karndeep.Stock.Reporting
{
    public interface IStockReport
    {
        string SecurityId { get; }
        DateTime? From { get; }
        DateTime? To { get; }
        IEnumerable<StockMetric> Metrics { get; }
    }
}
