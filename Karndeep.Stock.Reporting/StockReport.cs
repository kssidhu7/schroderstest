﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Karndeep.Stock.Reporting
{
    public class StockReport : IStockReport
    {
        public string SecurityId { get; internal set; }

        public DateTime? From { get; internal set; }

        public DateTime? To { get; internal set; }

        public IEnumerable<StockMetric> Metrics { get; internal set; }
    }
}
