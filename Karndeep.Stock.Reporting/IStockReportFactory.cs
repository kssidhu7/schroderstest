﻿using Karndeep.Stock.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Karndeep.Stock.Reporting
{
    public interface IStockReportFactory
    {
        IStockReport CreateReport(IStockData stockData, params int[] windowSizes);
    }
}
