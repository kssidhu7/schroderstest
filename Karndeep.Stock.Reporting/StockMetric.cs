﻿namespace Karndeep.Stock.Reporting
{
    public class StockMetric
    {
        // Can also have list of IMetric, will allow more customisable reports etc.
        public int WindowSize { get; internal set; }
        public decimal High { get; internal set; }
        public decimal Low { get; internal set; }
        public decimal MaxDrawdown { get; internal set; }
        public decimal ReturnOnInvestment { get; internal set; }
        public double SharpeRatio { get; internal set; }
        public double AverageReturn { get; internal set; }
        public double StandardDeviationReturns { get; internal set; }
    }
}