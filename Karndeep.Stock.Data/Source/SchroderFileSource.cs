﻿using CsvHelper;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karndeep.Stock.Data.Source
{
    public class SchroderFilteRecord
    {
        public string SecurityId { get; set; }
        public DateTime Timestamp { get; set; }
        public decimal Price { get; set; }
    }

    public class SchroderFileSource : IStockDataSource
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        public SchroderFileSource(string symbol, string fileUri)
        {
            SecurityId = symbol;
            Uri = new Uri(fileUri);
            if (!File.Exists(Uri.LocalPath))
                throw new FileNotFoundException($"Could not locate Schroder stock file at {fileUri}");
        }

        public string SecurityId { get; }
        public Uri Uri { get; }

        public Task<IStockData> GetStockDataAsync(string symbol, DateTime? from = null, DateTime? to = null)
        {
            return Task.Factory.StartNew<IStockData>(() =>
            {
                var taskId = TaskIdGenerator.GetNewTaskId();
                _logger.Info(taskId.CreateDiagnosticMessage($"Loading Schroder stock file for {symbol} from {from} to {to}"));
                var data = new StockData(symbol, from, to);
                using (var reader = File.OpenText(Uri.LocalPath))
                {
                    var csv = new CsvReader(reader);
                    var records = csv.GetRecords<SchroderFilteRecord>();
                    // Evalute price enumeration as we are inside using block
                    var fromDate = from ?? DateTime.MinValue;
                    var toDate = to ?? DateTime.MaxValue;
                    data.Prices = records
                        .Where(x => x.SecurityId == symbol && x.Timestamp >= fromDate && x.Timestamp <= toDate)
                        .Select(x => new PricePoint(x.Timestamp, x.Price))
                        .ToList();
                    _logger.Info(taskId.CreateDiagnosticMessage("Complete Schroder stock file load"));
                }
                return data;
            });
        }

        public bool HasStockData(string symbol)
        {
            return string.Equals(symbol, symbol, StringComparison.Ordinal);
        }
    }
}
