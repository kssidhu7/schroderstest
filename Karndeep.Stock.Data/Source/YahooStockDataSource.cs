﻿using System;
using System.Linq;
using System.Threading.Tasks;
using YahooFinanceApi;
using NLog;

namespace Karndeep.Stock.Data.Source
{
    public class YahooStockDataSource : IStockDataSource
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public async Task<IStockData> GetStockDataAsync(string symbol, DateTime? from = null, DateTime? to = null)
        {
            var taskId = TaskIdGenerator.GetNewTaskId();
            _logger.Info(taskId.CreateDiagnosticMessage($"Loading Yahoo stock data for {symbol} from {from} to {to}"));
            var history = await Yahoo.GetHistoricalAsync(symbol, from, to);
            var data = new StockData(symbol, from, to)
            {
                Prices = history.Select(x => new PricePoint(x.DateTime, x.AdjustedClose)).ToList()
            };
            _logger.Info(taskId.CreateDiagnosticMessage($"Complete Yahoo stock data load"));
            return data;
        }

        public bool HasStockData(string symbol) => true;
    }
}
