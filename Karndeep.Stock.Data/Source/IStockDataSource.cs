﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Karndeep.Stock.Data.Source
{
    public interface IStockDataSource
    {
        bool HasStockData(string symbol);
        Task<IStockData> GetStockDataAsync(string symbol, DateTime? from = null, DateTime? to = null);
    }
}
