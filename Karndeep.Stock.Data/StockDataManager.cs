﻿using Karndeep.Stock.Data.Source;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Karndeep.Stock.Data
{
    public class StockDataManager : IStockDataManager
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private readonly List<IStockDataSource> _sources = new List<IStockDataSource>();

        public void AddSchroderFileSource(string symbol, string fileUri, int priority = 0)
        {
            int idx = Math.Max(0, Math.Min(_sources.Count, priority));
            try
            {
                var source = new SchroderFileSource(symbol, fileUri);
                lock (_sources)
                    _sources.Insert(idx, source);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Failed to add Schroder source {symbol}: {fileUri}, Priority:{priority}");
            }
        }

        public void AddYahooSource(int priority = 0)
        {
            int idx = Math.Max(0, Math.Min(_sources.Count, priority));
            try
            {
                var source = new YahooStockDataSource();
                lock (_sources)
                    _sources.Insert(idx, source);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Failed to add Yahoo source");
            }
        }

        public async Task<IStockData> GetStockDataAsync(string symbol, DateTime? from = null, DateTime? to = null)
        {
            IStockData data = null;
            var taskId = TaskIdGenerator.GetNewTaskId();
            _logger.Info(taskId.CreateDiagnosticMessage($"Fetching stock data for {symbol}, from = {from}, to = {to}"));
            foreach (var source in _sources)
            {
                if (source.HasStockData(symbol))
                {
                    try
                    {
                        data = await source.GetStockDataAsync(symbol, from, to).TimeoutAfter(TimeSpan.FromSeconds(30));
                        if (data != null)
                        {
                            _logger.Info(taskId.CreateDiagnosticMessage($"Retreived data using {source.GetType()}"));
                            return data;
                        }
                    }
                    catch (TimeoutException)
                    {
                        continue;
                    }
                }
            }
            return data;
        }
    }
}
