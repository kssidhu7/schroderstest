﻿using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Karndeep.Stock.Data
{
    public struct StockDataLogMessage
    {
        public string Message;
        public TaskId? TaskId;
    }

    public static class Extensions
    {
        public static StockDataLogMessage CreateDiagnosticMessage(this TaskId taskId, string message) => new StockDataLogMessage { TaskId = taskId, Message = message };
        
        // https://stackoverflow.com/a/22078975
        public static async Task<TResult> TimeoutAfter<TResult>(this Task<TResult> task, TimeSpan timeout)
        {
            using (var timeoutCancellationTokenSource = new CancellationTokenSource())
            {
                var completedTask = await Task.WhenAny(task, Task.Delay(timeout, timeoutCancellationTokenSource.Token));
                if (completedTask == task)
                {
                    timeoutCancellationTokenSource.Cancel();
                    return await task;  // Very important in order to propagate exceptions
                }
                else
                {
                    throw new TimeoutException("The operation has timed out.");
                }
            }
        }
    }
}
