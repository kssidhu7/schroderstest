﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Karndeep.Stock.Data
{
    // This struct can be customised for wider range if needed
    public struct TaskId
    {
        public ulong Id;
        public override string ToString() => $"{Id}";
    }

    public static class TaskIdGenerator
    {
        private static long TaskId = long.MinValue;

        
        public static TaskId GetNewTaskId() => new TaskId { Id = (ulong)Interlocked.Increment(ref TaskId) };
    }
}
