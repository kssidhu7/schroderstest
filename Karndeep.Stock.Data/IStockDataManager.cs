﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Karndeep.Stock.Data
{
    public interface IStockDataManager
    {
        void AddSchroderFileSource(string symbol, string fileUri, int priority = 0);
        void AddYahooSource(int priority = 0);
        Task<IStockData> GetStockDataAsync(string symbol, DateTime? from = null, DateTime? to = null);
    }
}
