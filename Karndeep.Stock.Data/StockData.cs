﻿using System;
using System.Collections.Generic;

namespace Karndeep.Stock.Data
{
    public class StockData : IStockData
    {
        public string SecurityId { get; }

        public DateTime? From { get; }

        public DateTime? To { get; }

        public IReadOnlyList<PricePoint> Prices { get; internal set; }

        public StockData(string securityId, DateTime? from, DateTime? to)
        {
            SecurityId = securityId;
            From = from;
            To = to;
        }
    }
}
