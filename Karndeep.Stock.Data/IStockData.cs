﻿using System;
using System.Collections.Generic;

namespace Karndeep.Stock.Data
{
    public struct PricePoint
    {
        public DateTime Timestamp;
        public decimal Price;
        public PricePoint(DateTime timestamp, decimal price)
        {
            Timestamp = timestamp;
            Price = price;
        }
    }

    public interface IStockData
    {
        string SecurityId { get; }
        DateTime? From { get; }
        DateTime? To { get; }
        IReadOnlyList<PricePoint> Prices { get; }
    }
}
